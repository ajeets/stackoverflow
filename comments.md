`https://data.stackexchange.com/stackoverflow/query/75438/whats-my-pundit-badge-progress`

`[Did you just answer your own question?](https://stackoverflow.com/help/self-answer)`

`[Editing is allowed on SO](http://meta.stackoverflow.com/a/317303/2873538)`

`We will need some extra information such as X, Y and Z`

`It sounds like you may need to learn how to use a debugger to step through your code. With a good debugger, you can execute your program line by line and see where it is deviating from what you expect. This is an essential tool if you are going to do any programming. Further reading: [How to debug small programs](https://ericlippert.com/2014/03/05/how-to-debug-small-programs/).`

`When is this homework assignment due?`

`This smells like an [XY problem](http://mywiki.wooledge.org/XyProblem). What are you *really* trying to achieve?`

`Hi @user1234 if this or any answer has solved your question please consider [accepting it](http://meta.stackexchange.com/q/5234/179419) by clicking the check-mark. This indicates to the wider community that you've found a solution and gives some reputation to both the answerer and yourself. There is no obligation to do this.`

`[Please do not post images of code or data as they are difficult to read](http://meta.stackoverflow.com/a/285557/2873538).`

`@user1234 Your answer isn't clear to us. Could you make it little [clearer](http://meta.stackexchange.com/a/7659)?`

`Welcome to Stack Overflow! Please take the [tour](http://stackoverflow.com/tour), have a look around, and read through the [help center](http://stackoverflow.com/help), in particular *[How do I ask a good question?](http://stackoverflow.com/help/how-to-ask)*, *[What types of questions should I avoid asking?](http://stackoverflow.com/help/dont-ask)*, and *[What topics can I ask about here?](http://stackoverflow.com/help/on-topic)*`

`Your question doesn't provide any information that would allow to diagnose your problem.`

`Please work on your [accept rate](http://meta.stackexchange.com/a/16729).`

`You can [answer your own question](http://stackoverflow.com/help/self-answer) and mark this as the accepted answer.`

`This does not provide an answer to the question. Once you have sufficient [reputation](http://stackoverflow.com/help/whats-reputation) you will be able to [comment on any post](http://stackoverflow.com/help/privileges/comment); instead, [provide answers that don't require clarification from the asker](http://meta.stackexchange.com/questions/214173/why-do-i-need-50-reputation-to-comment-what-can-i-do-instead).`

`If you have a new question, please ask it by clicking the [Ask Question](http://stackoverflow.com/questions/ask) button. Include a link to this question if it helps provide context.`

`While this link may answer the question, it is better to include the essential parts of the answer here and provide the link for reference. Link-only answers can become invalid if the linked page changes.`

`A link to a potential solution is always welcome, but please [add context around the link](http://meta.stackoverflow.com/a/8259/169503) so your fellow users will have some idea what it is and why it�s there. Always quote the most relevant part of an important link, in case the target site is unreachable or goes permanently offline. Take into account that being barely more than a link to an external site is a possible reason as to [Why and how are some answers deleted?](http://stackoverflow.com/help/deleted-answers).`

`*One Ring to rule them all, One Ring to find them, One Ring to bring them all and in the darkness bind them - Tolkien*`

`Just add the [Minimal, Complete, and Verifiable example](http://stackoverflow.com/help/mcve); not the full code.`

`Asking [perfect question](https://codeblog.jonskeet.uk/2010/08/29/writing-the-perfect-question/)`

